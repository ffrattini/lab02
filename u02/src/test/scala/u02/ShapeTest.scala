package u02
import org.junit.Assert.*
import org.junit.Test
import u02lab.es7.*
import u02lab.es7.Shape.*

class ShapeTest {

  val rectangle = Rectangle(12, 30)
  val square = Square(12.6)
  val circle = Circle(8.75)

  @Test def testPerimeter(): Unit =
    assertEquals(84.0, perimeter(rectangle), 0.001)
    assertEquals(54.976, perimeter(circle), 0.001)
    assertEquals(50.4, perimeter(square), 0.001)
    assertEquals(110.895, perimeter(Shape.Circle(15+2.65)), 0.001)

  @Test def testArea(): Unit =
    assertEquals(360, area(rectangle), 0.001)
    assertEquals(240.521, area(circle), 0.001)
    assertEquals(158.76, area(square), 0.001)
    assertEquals(106.25, area(Shape.Rectangle(20 - 3, 1.25 * 5)), 0.001)

}
