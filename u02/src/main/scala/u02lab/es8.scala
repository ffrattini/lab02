package u02lab

object es8 extends App:

  enum Option[A]:
    case Some(a: A)
    case None()

  object Option:

    def isEmpty[A](opt: Option[A]): Boolean = opt match
      case None() => true
      case _ => false

    def orElse[A, B >: A](opt: Option[A], orElse: B): B = opt match
      case Some(a) => a
      case _ => orElse

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match
      case Some(a) => f(a)
      case _ => None()

    def filter[A](o: Option[A])(f: A => Boolean): Option[A] = o match
      case Some(a) if f(a) => Some(a)
      case _ => None()

    def map[A](o: Option[A])(f: A => Boolean): Option[Boolean] = o match
      case Some(a) if f(a) => Some(true)
      case Some(a) if !f(a) => Some(false)
      case _ => None()

    def map2[A, B](o1: Option[A])(o2: Option[B]): Option[(A, B)] = o1 match
      case Some(a) => o2 match
        case Some(b) => Some(a, b)
        case _ => None()
      case _ => None()


  import Option.*

  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(Some(5))(_ > 8)) // Some(false)
  println(map(None[Int]())(_ > 2)) // None

  println(filter(Some(5))(_ > 2)) //Some(5)
  println(filter(Some(5))(_ > 8)) //None
  println(filter(None[Int]())(_ > 2)) //none

  println(map2(Some(5))(Some(24)))  //Some((5, 24))
  println(map2(Some(true))(Some("hello"))) //Some((true, hello))
  println(map2(None())(Some(2)))  //None()

