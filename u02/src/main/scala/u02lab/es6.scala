package u02lab

object es6 extends App:

  def fibonacci (n: Int): Int =
    @annotation.tailrec
    def _fib(n: Int, first: Int, second: Int, acc: Int): Int = n match
        case 0 | 1 => acc
        case _ => _fib(n - 1, second, first + second, first + second)

    _fib(n, 0, 1, 0)
  
  println((fibonacci(0), fibonacci(1), fibonacci(2), fibonacci(3), fibonacci(4)))
  println((fibonacci(5), fibonacci(6), fibonacci(7), fibonacci(8), fibonacci(9)))
  println((fibonacci(10), fibonacci(11), fibonacci(12), fibonacci(13), fibonacci(14)))
