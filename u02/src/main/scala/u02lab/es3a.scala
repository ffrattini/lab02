package u02lab

object es3a extends App :

  //anonymous function
  val parity: Int => String = _ match
    case n if n % 2 == 0 => "even"
    case n if n % 2 != 0 => "odd"

  println(parity(11)) //odd
  println(parity(-256)) //even

  //defined function
  def checkPar(n: Int): String = n % 2 match
    case 0 => "even"
    case 1 => "odd"

  println(checkPar(24)) //even
  println(checkPar(123456)) //even
