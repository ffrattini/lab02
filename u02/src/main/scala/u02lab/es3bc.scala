package u02lab

object es3bc extends App :
  val empty: String => Boolean = _ == "" // predicate on strings
  val neg: (String => Boolean) => String => Boolean = f => !f(_)
  val notEmpty = neg(empty)
  println(notEmpty("foo")) // true
  println(notEmpty("")) // false
  println(notEmpty("foo") && !notEmpty("")) // true

  def negationString(f: String => Boolean): String => Boolean = !f(_)

  def negationGeneric[X](f: X => Boolean): X => Boolean = !f(_)

  def emptyGeneric[X]: X => Boolean = _ == ""

  val checkGeneric = negationGeneric(emptyGeneric)
  val checkIfFull = negationString(empty);
  println(checkGeneric(2)) //true
  println(checkGeneric("")) //false
  println(checkGeneric(true)) //true
  println(checkIfFull("ciao")) //true
  println(checkIfFull("")) //false
  println(!checkIfFull("pieno") && checkIfFull("")) //false
