package u02lab

object es4 extends App :

  val p1: Double => Double => Double => Boolean = x => y => z => x <= y && y <= z

  println(p1(2)(3)(4)) //true
  println(p1(3)(10)(1)) //false

  val p2: (Double, Double, Double) => Boolean = (x, y, z) => x <= y && y <= z

  println(p2(1,2,3))  //true
  println(p2(4,3,2))  //false
  println(p2(2,2,2))  //true

  def p3 (x: Double)(y: Double)(z: Double): Boolean = x <= y && y <= z

  println(p3(1)(2)(3))  //true
  println(p3(4)(3)(2))  //false
  println(p3(2)(2)(2))  //true

  def p4 (x: Double, y: Double, z: Double): Boolean = x <= y && y <= z

  println(p4(12, 54, 100))  //true
  println(p4(45, 3, 189)) //false
  println(p4(1, 2, 3) && !p4(98, 34, 15))  //true