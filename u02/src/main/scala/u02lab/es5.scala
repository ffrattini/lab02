package u02lab

object es5 extends App:

  def compose (f: Int => Int, g: Int => Int): Int => Int = f compose g

  println(compose(_ - 1, _ * 2)(5)) //9
  println(compose(67 + _ - 100, _ / 3 * 5)(9)) //-18

  def composeGeneric[X](f: X => X, g: X => X): X => X = f compose g

  println(composeGeneric(true == _, false != _)(true))  //true
  println(composeGeneric("helloworld" == _, "hello" + _)("world"))  //true
  println(composeGeneric("Hi everyone\n" + _, "my name is " + _)("Bob"))  //Hi everyone\nmy name is bob
  println(composeGeneric[Int](5 + _, 6 * _)(5)) //35
