package u02lab

object es7:
  enum Shape:
    case Rectangle(height: Double, width: Double)
    case Circle(radius: Double)
    case Square(side: Double)

  def perimeter(s: Shape): Double = s match
    case Shape.Rectangle(h, w) => 2 * w + 2 * h
    case Shape.Circle(r) => 2 * r * 3.1415
    case Shape.Square(s) => s * 4

  def area(s: Shape): Double = s match
    case Shape.Rectangle(h, w) => h * w
    case Shape.Circle(r) => r * r * 3.1415
    case Shape.Square(s) => s * s


